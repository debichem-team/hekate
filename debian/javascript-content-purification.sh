#!/bin/sh

# All the files in the source tree that are actually installed in the
# form of links to files installed by the system from other packages,
# will be removed.

# First the user is shown the list and asked to validate. If correct,
# then the files are actually removed from the source tree.

workDir="${HOME}/devel/packaging/hekate/development"

IFS="
"

firstShow()
{
    echo "This is the list of commands that will be issued."
    
    for line in $(cat ${workDir}/debian/hekate.links)
    do
        filePath=$(echo ${line} \
            | sed 's|^/usr/share/.*[ ]*\(usr/share/.*$\)|\1|g' \
            | awk '{print $2}' | sed 's|usr/share/hekate/www/||g')
        echo "rm ${filePath}"
    done
}

nowDo()
{
    for line in $(cat ${workDir}/debian/hekate.links)
    do
        filePath=$(echo ${line} \
            | sed 's|^/usr/share/.*[ ]*\(usr/share/.*$\)|\1|g' \
            | awk '{print $2}' | sed 's|usr/share/hekate/www/||g')
        rm -v ${filePath}
    done
}

firstShow

echo "Is it ok to run these commands (y/n) ?"

read answer

if [ "${answer}" = "y" -o "${answer}" = "Y" ]
then
    nowDo
else
    echo "Aborted."
    exit
fi
